/*
 * The MIT License
 *
 * Copyright 2018 David Figueroa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mcd;

import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author David
 */
public class MCD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);//Objeto que se encarga de leer los datos ingresados por el usuario
        System.out.println("Ingrese el primer número");
        BigInteger a = sc.nextBigInteger(); //Lee el valor de a
        System.out.println("Ingrese el segundo número");
        BigInteger b = sc.nextBigInteger(); //Lee el valor de b
        long mcdIni = System.nanoTime();   //Devuelve el tiempo en nanosegundos que lleva el programa corriendo
        BigInteger mcdClasico = mcd(a, b);
        long mcdFin = System.nanoTime();
        System.out.println("El algoritmo clásico retornó: " + mcdClasico + " y tardó " + (mcdFin - mcdIni) + " nanosegundos");
        long euclidesIni = System.nanoTime();   //Devuelve el tiempo en nanosegundos que lleva el programa corriendo
        BigInteger mcdEuclides = euclides(a, b);
        long euclidesFin = System.nanoTime();
        System.out.println("El algoritmo de Euclides retornó: " + mcdEuclides + " y tardó " + (euclidesFin - euclidesIni) + " nanosegundos");
    }

    /**
     * Función para alcular el Máximo Común Divisor de dos números según la
     * definición de éste.
     *
     * @param a Primer valor a calcular el Máximo Común Divisor
     * @param b Segundo valor a calcular el Máximo Común Divisor
     * @return Máximo Común Divisor entre a y b
     */
    public static BigInteger mcd(BigInteger a, BigInteger b) {
        BigInteger i = a.min(b).add(BigInteger.ONE);
        do {
            i = i.subtract(BigInteger.ONE);
        } while (!(b.mod(i).compareTo(BigInteger.ZERO) == 0 && a.mod(i).compareTo(BigInteger.ZERO) == 0));
        return i;
    }

    /**
     * Función que resuelve el problema de Máximo Común Divisor de dos números
     * por medio del algoritmo de Euclides
     *
     * @param a Primer valor a calcular el Máximo Común Divisor
     * @param b Segundo valor a calcular el Máximo Común Divisor
     * @return Máximo Común Divisor entre a y b
     */
    public static BigInteger euclides(BigInteger a, BigInteger b) {
        while (a.compareTo(BigInteger.ZERO) > 0) {
            BigInteger t = a;
            a = b.mod(a);
            b = t;
        }
        return b;
    }
}
